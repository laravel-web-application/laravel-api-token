# Laravel API Token
### Things todo list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-api-token.git`
2. Go inside the folder: `cd laravel-api-token`
3. Run `cp .env.example .env` then set your desired database username & password
4. Run `php artisan migrate`
5. Run `php artisan db:seed --class=UserSeeder`
6. Run `php artisan serve`
7. Open your REST API Client such as (POSTMAN or INSOMNIA)

### Screen shot

API Token

![Get User](img/api.png "Get User")
