<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = "Hatake Kakashi";
        $user->email = "hatake_kakashi@konohagakure.co.jp";
        $user->password = Hash::make("rahasiakitaberdua");
        $user->api_token = Str::random(100);

        $user->save();
    }
}
